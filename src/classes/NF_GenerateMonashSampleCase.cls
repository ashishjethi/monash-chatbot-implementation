/**
* Created by Neuraflash LLC on 25/02/19.
* Implementation : Chatbot
* Summary : Create a case record
* 
*/
global without sharing class NF_GenerateMonashSampleCase implements nfchat.PreInvokeClassAIMethodService {
    public Case caseCheck;
    public NF_GenerateMonashSampleCase(){
        caseCheck = new Case();
    }
    
    public List<String> ReturnStringResponse(nfchat.AIParameters data){
        Id caseId;
        list<string> tempResultFormatter = new list<string>();
        try{
            nfchat__Chat_Log__c log = [SELECT Id, Name, Case_Subject__c , Date_Of_Birth__c, Description__c , nfchat__First_Name__c,nfchat__Last_Name__c,
                                nfchat__Email__c, ID_Number__c, Account_Type__c
                                   FROM nfchat__Chat_Log__c
                                   WHERE nfchat__Session_Id__c = :data.sessionId limit 1];
            List<Contact> conList = [SELECT id FROM Contact WHERE Monash_Email_Address__c =: log.nfchat__Email__c or Person_ID_unique__c =: log.ID_Number__c limit 1];
            if(!conList.isEmpty()){ //contact found
                caseId = NF_GenerateMonashSampleCase.createCase(conList[0],log);
            }
            else{
                Contact con = new Contact();
                con.FirstName = log.nfchat__First_Name__c;
                con.LastName = log.nfchat__Last_Name__c;
                if(log.Account_Type__c == 'Student'){
                    con.Monash_Email_Address__c = log.nfchat__Email__c;
                    con.Person_ID_unique__c = log.ID_Number__c;
                    con.Birthdate = NF_GenerateMonashSampleCase.setStringToDateFormat(log.Date_Of_Birth__c);
                }
                else if(log.Account_Type__c == 'Staff'){
                    con.Monash_Email_Address__c = log.nfchat__Email__c;
                    con.SAP_Staff_Id__c = log.ID_Number__c;
                    con.Birthdate = NF_GenerateMonashSampleCase.setStringToDateFormat(log.Date_Of_Birth__c);
                }
                else{
                    con.Personal_Email__c = log.nfchat__Email__c;
                }
                insert con;
                if(con.Id != null){
                    caseId = NF_GenerateMonashSampleCase.createCase(con,log);
                }
            }
            map<String, Object> paramsMap = (map<String, Object>)JSON.deserializeUntyped(data.params);
            string promptMessage = (String)paramsMap.get('prompt'); 
            if(String.isNotBlank(caseId)){
                caseCheck = [SELECT CaseNumber FROM case WHERE id=: caseId limit 1];
                promptMessage = promptMessage.replace('CASENUMBER',caseCheck.CaseNumber);
                tempResultFormatter.add(promptMessage);
            }
        }
        catch (DmlException e) {
            System.debug('Exception Caught: '+e.getMessage());
            return null;
        }
        return tempResultFormatter;
    }
    private static Id createCase(Contact con, nfchat__Chat_Log__c log){
        Case caseRecord = new Case();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Enquiry').getRecordTypeId();
        caseRecord.RecordTypeId = recordTypeId;
        if(!String.isBlank(log.Case_Subject__c)){
            caseRecord.Subject = log.Case_Subject__c;
        }
        if(!String.isBlank(log.Description__c)){
            caseRecord.Description = log.Description__c;
        }
        caseRecord.ContactId = con.Id;
        caseRecord.nfchat__Chat_Log__c = log.id;
        if(caseRecord.id == null){
            try{
                insert caseRecord;
            }catch (DmlException e) {
                System.debug('Exception Caught: '+e.getMessage());
                return null;
            }
        }
        return caseRecord.Id;
    }
    private static Date setStringToDateFormat(String myDate) {
        String[] strDate = myDate.split('/');
        Integer myIntDate = integer.valueOf(strDate[0]);
        Integer myIntMonth = integer.valueOf(strDate[1]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        return d;
    }   
}