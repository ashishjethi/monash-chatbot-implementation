@isTest
public class NF_GenerateMonashSampleCaseTest {
	static testMethod void CreateCaseRecord_Student() {
        nfchat__Chat_Log__c log = NF_TestDataUtill.createChatLog();
        log.Account_Type__c = 'Student';
        insert log;
		test.startTest();
		NF_GenerateMonashSampleCase caseGenerationInstance = new NF_GenerateMonashSampleCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, Subject, Description from Case]);
		system.assertEquals(1,caseList.size());
	}
    static testMethod void CreateCaseRecord_Staff() {
        nfchat__Chat_Log__c log = NF_TestDataUtill.createChatLog();
        log.Account_Type__c = 'Staff';
        insert log;
		test.startTest();
		NF_GenerateMonashSampleCase caseGenerationInstance = new NF_GenerateMonashSampleCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, Subject, Description from Case]);
		system.assertEquals(1,caseList.size());
	}
    static testMethod void CreateCaseRecord_Neither() {
        nfchat__Chat_Log__c log = NF_TestDataUtill.createChatLog();
        log.Account_Type__c = 'Neither';
        insert log;
		test.startTest();
		NF_GenerateMonashSampleCase caseGenerationInstance = new NF_GenerateMonashSampleCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, Subject, Description from Case]);
		system.assertEquals(1,caseList.size());
	}
    static testMethod void CreateCaseRecord_withContact() {
        nfchat__Chat_Log__c log = NF_TestDataUtill.createChatLog();
        log.Account_Type__c = 'Student';
        insert log;
        Contact con = NF_TestDataUtill.createContact();
        con.Monash_Email_Address__c = 'joySmith@monash.edu';
        insert con;
		test.startTest();
		NF_GenerateMonashSampleCase caseGenerationInstance = new NF_GenerateMonashSampleCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, Subject, Description from Case]);
		system.assertEquals(1,caseList.size());
	}
}