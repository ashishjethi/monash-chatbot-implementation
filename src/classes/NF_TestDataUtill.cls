@isTest
public class NF_TestDataUtill {
	public static nfchat__Chat_Log__c createChatLog(){
        nfchat__Chat_Log__c chatLog = new nfchat__Chat_Log__c();
        chatLog.nfchat__First_Name__c = 'Joy';
        chatLog.ID_Number__c = '11223344';
        chatLog.Description__c = 'Description';
        chatLog.Case_Subject__c  = 'Test Subject';
        chatLog.nfchat__Last_Name__c = 'Smith';
        chatLog.nfchat__Email__c = 'joySmith@monash.edu';
        chatLog.nfchat__AI_Config_Name__c = 'AIConfig Test';
        chatLog.nfchat__Session_Id__c = '1234474y54734y';
        chatLog.Date_Of_Birth__c = '01/01/2020';
        return chatLog;
    }
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        return acc;
    }
    public static Contact createContact(){
        Contact con = new Contact();
        Account acc = NF_TestDataUtill.createAccount();
        insert acc;
        con.AccountId = acc.id;
        con.Email = 'test@test.com';
        con.FirstName = 'test';
        con.LastName = 'contact';
        return con;
    }
}